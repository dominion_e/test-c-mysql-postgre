﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Data;

namespace Test_2
{
    class MYSQLCon
    {
            static string server = "localhost";
            static string user;
            static string password;
            static string port;

            public static string GetConnection(string user, string port, string password)
            {
                string conn = @"server=" + server + ";userid=" + user +  ";port=" + port + ";password=" + password + ";";
                return conn;
            }

            public static bool OpenConnection(string conn)
            {
            try{
                MySqlConnection con = new MySqlConnection(conn);
                con.Open();
                return true;
                }
                catch(Exception e)
                {
                    return false;
                }
            }

            public static bool CloseConnection(string conn)
        {
            try
            {
                MySqlConnection con = new MySqlConnection(conn);
                con.Close();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }


            public static DataTable RunQuery(string conn, string query)
            {
                MySqlConnection connection = null;
            try
            {
                connection = new MySqlConnection(conn);
                connection.Open();
                MySqlCommand cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = query;
                cmd.Prepare();

                MySqlDataAdapter adapter = new MySqlDataAdapter();
                adapter.SelectCommand = cmd;
                DataTable dTable = new DataTable();
                adapter.Fill(dTable);

               return dTable;
            }
            finally
            {
                connection.Close();
            }
       }
    }
}
