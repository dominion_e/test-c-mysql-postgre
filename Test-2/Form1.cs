﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Test_2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void connect_Click_1(object sender, EventArgs e)
        {
            try
            {
                string rdbms = RDBMS.SelectedItem.ToString();
                string user = User.Text;
                string password = Password.Text;
                string port = Port.Text;

                string con;

                if (rdbms == "MySQL")
                {
                    con = MYSQLCon.GetConnection(user, port, password);
                    MYSQLCon.OpenConnection(con);
                    Form2 f2 = new Form2();
                    f2.enteredcon = con;
                    f2.dbms = rdbms;
                    this.Hide();
                    f2.Show();
                    MYSQLCon.CloseConnection(con);

                }
                else if (rdbms == "PostGreSQL")
                {
                    con = PostgreCon.GetConnection(user, port, password);
                    PostgreCon.OpenConnection(con);
                    Form2 f2 = new Form2();
                    f2.enteredcon = con;
                    f2.dbms = rdbms;
                    this.Hide();
                    f2.Show();
                    PostgreCon.CloseConnection(con);
                }
            }
            catch (Exception ex) {
                MessageBox.Show("Unable to connect to DB. Error:" + ex);
                    }
            }


    }
}
