﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Test_2
{
    public partial class Form2 : Form
    {
        public string dbms { get; set; }
        public string enteredcon { get; set; }
        public Form2()
        {
            InitializeComponent();
            db.Text = dbms;
        }
        private void execute_Click(object sender, EventArgs e)
        {
            DataTable dt;
            string con;
            string rdbms = dbms;
            string database = db.Text.Trim();
            if (String.IsNullOrEmpty(database))
            {
                con = enteredcon;
            }
            else
            {
                con = enteredcon + "database=" + database + ";";
            }
            
            string query = Query.Text;
            switch (rdbms)

            {
                case "MySQL":
                    dt = MYSQLCon.RunQuery(con, query);
                    dataGridView1.DataSource = dt;
                    break;
                case "PostgreSQL":
                    dt = PostgreCon.RunQuery(con, query);
                    break;
                default:
                    break; 

            }
            
            
        }
       
    }
}
