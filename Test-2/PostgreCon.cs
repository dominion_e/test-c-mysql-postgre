﻿using System;
using System.Collections.Generic;
using System.Linq;
using Npgsql;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace Test_2
{
    class PostgreCon
    {
        static string server = "localhost";
        static string user;
        static string password;
        static string port;

        public static string GetConnection(string user, string port, string password)
        {
            string conn = @"server=" + server + ";userid=" + user + ";port=" + port + ";password=" + password + ";";
            return conn;
        }

        public static bool OpenConnection(string conn)
        {
            try
            {
                NpgsqlConnection con = new NpgsqlConnection(conn);
                con.Open();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public static bool CloseConnection(string conn)
        {
            try
            {
                NpgsqlConnection con = new NpgsqlConnection(conn);
                con.Close();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public static DataTable RunQuery(string conn, string query)
        {
            NpgsqlConnection connection = null;
            try
            {
                connection = new NpgsqlConnection(conn);
                connection.Open();
                NpgsqlCommand cmd = new NpgsqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = query;
                cmd.Prepare();

                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter();
                adapter.SelectCommand = cmd;
                DataTable dTable = new DataTable();
                adapter.Fill(dTable);

                return dTable;
            }
            finally
            {
                connection.Close();
            }
        }
    }
}
